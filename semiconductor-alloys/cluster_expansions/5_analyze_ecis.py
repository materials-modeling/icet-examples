# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from collections import OrderedDict
from glob import glob
from icet import ClusterExpansion
from numpy import count_nonzero
from os.path import basename

units = {'mixing_energy': 'meV',
         'direct_bandgap': 'eV',
         'indirect_bandgap': 'eV',
         'lattice_parameter': 'A',
        }

for fname in glob('backup/*.ce'):
    prop = basename(fname).replace('.ce', '')
    unit = units[prop]
    ce = ClusterExpansion.read(fname)

    # step 1: Collect ECIs in dictionary
    ecis = OrderedDict()
    for order in range(len(ce.cluster_space.cutoffs)+2):
        for orbit in ce.cluster_space.orbit_data:
            if orbit['order'] != order:
                continue
            if order not in ecis:
                ecis[order] = {'radius': [], 'parameters': []}
            ecis[order]['radius'].append(orbit['radius'])
            ecis[order]['parameters'].append(ce.parameters[orbit['index']])

    # step 2: Plot ECIs
    fig, axs = plt.subplots(1, 3, sharey=True, figsize=(7.5, 3))
    for k, (order, data) in enumerate(ecis.items()):
        if k < 2 or k > 4:
            continue
        ax = axs[k-2]
        ax.set_xlim((1, 5))
        ax.set_xlabel(r'Cluster radius (Å)')
        if order == 2:
            ax.set_ylabel(f'Effective cluster interaction ({unit})')
        if order == 4:
            ax.text(0.05, 0.55, f'zerolet: {ecis[0]["parameters"][0]:.1f} {unit}',
                    transform=ax.transAxes)
            ax.text(0.05, 0.45, f'singlet: {ecis[1]["parameters"][0]:.1f} {unit}',
                    transform=ax.transAxes)
        ax.bar(data['radius'], data['parameters'], width=0.05)
        ax.scatter(data['radius'], len(data['radius']) * [0],
                   marker='o', s=2.0)
        ax.plot([0, 5], [0, 0], color='black', lw=0.5)
        ax.text(0.05, 0.91, 'order: {}'.format(order),
                transform=ax.transAxes)
        ax.text(0.05, 0.81, '#parameters: {}'.format(len(data['parameters'])),
                transform=ax.transAxes,)
        ax.text(0.05, 0.71, '#non-zero params: {}'
                .format(count_nonzero(data['parameters'])),
                transform=ax.transAxes,)
    plt.savefig(f'figs/params_{prop}.png', bbox_inches='tight')
