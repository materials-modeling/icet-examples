import matplotlib.pyplot as plt
from glob import glob
from icet import ClusterExpansion
from icet.tools import ConvexHull, enumerate_structures
from os.path import basename

# step 1: Setup
species_for_enumeration = [['Ga', 'Al'], ['As']]
ylabels = {'mixing_energy': 'Mixing energy (meV/site)',
           'direct_bandgap': 'Direct bandgap (eV)',
           'indirect_bandgap': 'Indirect bandgap (eV)',
           'lattice_parameter': 'Lattice parameter (A)'}

# step 2: Load cluster expansions
ces = {}
for fname in glob('backup/*.ce'):
    prop = basename(fname).replace('.ce', '')
    ces[prop] = ClusterExpansion.read(fname)
    chemical_species = ces[prop].cluster_space.chemical_symbols
    prim = ces[prop].cluster_space.primitive_structure
    print(f'ce: {prop:18}  chemical_species: {chemical_species}')

# step 3: Predict properties for enumerated structures
data = {'concentration': []}
for prop in ces:
    data[prop] = []
structures = []
for atoms in enumerate_structures(atoms=prim, sizes=range(1, 13),
                                  species=species_for_enumeration):
    nsites = len(atoms) / 2
    conc = atoms.get_chemical_symbols().count('Al') / nsites
    data['concentration'].append(conc)
    for prop, ce in ces.items():
        data[prop].append(ce.predict(atoms))
    structures.append(atoms)
print('Predicted properties for {} structures'.format(len(structures)))

# step 4: Construct convex hull
hull = ConvexHull(data['concentration'], data['mixing_energy'])

# step 5: Plot the results
for prop in ces:
    fig, ax = plt.subplots(figsize=(4, 3))
    ax.set_xlabel('Concentration')
    ax.set_ylabel(ylabels[prop])
    ax.set_xlim([0, 1])
    ax.scatter(data['concentration'], data[prop], marker='x')
    if prop == 'mixing_energy':
        ax.plot(hull.concentrations, hull.energies, '-o', color='green')
    plt.savefig(f'figs/prediction_{prop}.png', bbox_inches='tight')
