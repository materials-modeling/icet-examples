from ase.db import connect
from icet import (ClusterSpace, StructureContainer,
                  CrossValidationEstimator, ClusterExpansion)

# step 1: Basic setup
db = connect('databases/reference_data.db')
prim = db.get(id=1).toatoms()  # primitive structure

try:
    # step 2: Read the structue container from file
    sc = StructureContainer.read('backup/structures.sc')
except:
    # step 2a: Set up the basic structure and a cluster space
    allowed_species = ['Ga', 'Al', 'As']
    cs = ClusterSpace(atoms=prim,
                      cutoffs=[10, 9, 7],
                      chemical_symbols=[['As'], ['Ga', 'Al']])
    print(cs)

    # step 2b: Parse the input structures and set up a structure container
    sc = StructureContainer(cluster_space=cs)
    for row in db.select():
        if not all([c in allowed_species for c in row.count_atoms()]):
            continue
        sc.add_structure(atoms=row.toatoms(),
                         user_tag=row.tag,
                         properties={'mixing_energy': row.mixing_energy,
                                     'direct_bandgap': row.direct_bandgap,
                                     'indirect_bandgap': row.indirect_bandgap,
                                     'lattice_parameter': row.lattice_parameter})
    print(f'Added {len(sc)} structures')

    # step 2c: Write structure container to file
    sc.write('backup/structures.sc')
print(sc)

# step 4: Train CEs
for prop in ['mixing_energy', 'direct_bandgap',
             'indirect_bandgap', 'lattice_parameter']:
    opt = CrossValidationEstimator(fit_data=sc.get_fit_data(key=prop),
                                   fit_method='rfe-l2')
    opt.validate()
    opt.train()
    print(f'property: {prop}')
    print(opt)
    print('')
    ce = ClusterExpansion(cluster_space=sc.cluster_space,
                          parameters=opt.parameters)
    ce.write(f'backup/{prop}.ce')
