from ase.db import connect
from icet.tools import map_structure_to_reference
from numpy import where

# step 1: Get reference energies for the elements
db = connect('databases/dft_data.db')
erefs = {}
for row in db.select('natoms=2'):
    erefs[row.formula.replace('As', '')] = row.energy / (row.natoms / 2)

# step 2: Compile input data for CE construction
prim = db.get(id=1).toatoms()
db_new = connect('databases/reference_data.db')
for row in db.select():

    tag = row.filename.replace('run_', '').replace('.xml.gz', '')
    try:
        db_new.get(tag=tag)
        print(f'Skipping row due to existing tag: {tag}')
    except KeyError:
        print(f'Adding tag: {tag}')

        # map (relaxed) structure to ideal lattice
        atoms, _, _ = map_structure_to_reference(row.toatoms(), prim, 0.5)

        # mixing energy
        nsites = row.natoms / 2
        concentrations = {}
        mixing_energy = row.energy / nsites
        for elem, eref in erefs.items():
            concentrations[elem] = row.count_atoms().get(elem, 0) / nsites
            mixing_energy -= concentrations[elem] * eref
        mixing_energy *= 1e3

        # band gap
        VBM, CBM = -1e6, 1e6
        direct_bandgap = 1e6
        for kpt in row.data['kpoints']:
            kpt_VBM = max(where(kpt['occupations'] > 1.0,
                                kpt['energies'], -1e6))
            kpt_CBM = min(where(kpt['occupations'] < 1.0,
                                kpt['energies'], +1e6))
            direct_bandgap = min(kpt_CBM - kpt_VBM, direct_bandgap)
            VBM = max(VBM, kpt_VBM)
            CBM = min(CBM, kpt_CBM)
        indirect_bandgap = CBM - VBM

        # compute average lattice parameter
        alat = (4 * row.volume / row.natoms) ** (1 / 3)

        # add structure to database
        db_new.write(atoms,
                     tag=tag,
                     direct_bandgap=direct_bandgap,
                     indirect_bandgap=indirect_bandgap,
                     mixing_energy=mixing_energy,
                     data={'concentrations': concentrations},
                     lattice_parameter=alat)
