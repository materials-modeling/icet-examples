import matplotlib.pyplot as plt
from ase.db import connect
from icet import ClusterExpansion

# step 1: Set up data dictionary
data = {'concentration': []}
properties = ['mixing_energy', 'direct_bandgap',
              'indirect_bandgap', 'lattice_parameter']
ylabels = {'mixing_energy': 'Mixing energy (meV/site)',
           'direct_bandgap': 'Direct bandgap (eV)',
           'indirect_bandgap': 'Indirect bandgap (eV)',
           'lattice_parameter': 'Lattice parameter (A)'}
for prop in properties:
    data[f'reference_{prop}'] = []
    data[f'predicted_{prop}'] = []

# step 2: Compile reference data for plotting
allowed_species = ['Ga', 'Al', 'As']
db = connect('databases/reference_data.db')
for row in db.select():
    if not all([c in allowed_species for c in row.count_atoms()]):
        continue
    nsites = sum([n
                  for c, n in row.count_atoms().items()
                  if c in ['As']])
    conc_Al = row.count_atoms().get('Al', 0) / nsites
    data['concentration'].append(conc_Al)
    data['reference_mixing_energy'].append(row.mixing_energy)
    data['reference_direct_bandgap'].append(row.direct_bandgap)
    data['reference_indirect_bandgap'].append(row.indirect_bandgap)
    data['reference_lattice_parameter'].append(row.lattice_parameter)

# step 3: Compile predicted data for plotting
for prop in properties:
    ce = ClusterExpansion.read(f'backup/{prop}.ce')
    for row in db.select():
        if not all([c in allowed_species for c in row.count_atoms()]):
            continue
        data[f'predicted_{prop}'].append(ce.predict(row.toatoms()))

# step 4: Plot results
for prop in properties:
    print(f'property: {prop}')
    fig, ax = plt.subplots(figsize=(4, 3))
    ax.set_xlabel(r'Al concentration')
    ax.set_ylabel(ylabels[prop])
    ax.set_xlim([0, 1])
    ax.scatter(data['concentration'], data[f'reference_{prop}'],
               marker='o', label='reference')
    ax.scatter(data['concentration'], data[f'predicted_{prop}'],
               marker='x', label='CE prediction')
    ax.legend()
    plt.savefig(f'figs/comparison_{prop}.png', bbox_inches='tight')
