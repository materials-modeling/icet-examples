#!/usr/bin/env python

from __future__ import print_function, division
import os
import tarfile
import shutil
import argparse
import gzip

parser = argparse.ArgumentParser(description='Extract data files from'
                                 ' tgz-archived runs.')
parser.add_argument('files', nargs='+', action='append',
                    help='list of input files')
defval = 'out'
parser.add_argument('-o', '--outdir',
                    default=defval,
                    help='output directory [default: {}]'.format(defval))
args = parser.parse_args()
args.files = args.files[0]

for fname in args.files:
    with tarfile.open(fname, 'r') as tar:
        basename = os.path.basename(fname)
        basedir = basename.replace('.tgz', '').replace('.tar.gz', '')
        try:
            os.mkdir(args.outdir)
        except:
            pass

        for tag in ['OUTCAR', 'vasprun.xml']:
            origfile = '{}/{}'.format(basedir, tag)
            try:
                tar.extract(origfile)
            except:
                print('Failed to extract {} from {}'.format(tag, fname))
                continue

            if tag == 'vasprun.xml':
                targetfile = '{}/run_{}.xml'.format(args.outdir,
                                                       basedir)
            else:
                targetfile = '{}/{}.{}'.format(args.outdir, tag,
                                               basedir)
            print('  {} --> {}'.format(origfile, targetfile))
            shutil.move(origfile, targetfile)

            with open(targetfile, 'rb') as f_in, \
                    gzip.open('{}.gz'.format(targetfile), 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
            try:
                os.remove(targetfile)
            except:
                pass

        try:
            os.rmdir(basedir)
        except:
            pass
