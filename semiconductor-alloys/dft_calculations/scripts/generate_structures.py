from ase.build import bulk
from ase.io import write
from icet.tools import enumerate_structures

species = [['Al', 'Ga'], ['As']]
species = [['Al', 'Ga', 'In'], ['As']]
species = [['Al', 'Ga'], ['As', 'Sb']]
tag = ''.join([e[0].lower() for lst in species for e in sorted(lst)])
prim = bulk('GaAs', crystalstructure='zincblende', a=5.65)
structures = []
for k, atoms in enumerate(
        enumerate_structures(atoms=prim, sizes=range(1, 7), species=species)):
    write(f'POSCAR-{tag}{k:06d}', atoms, sort=True)
