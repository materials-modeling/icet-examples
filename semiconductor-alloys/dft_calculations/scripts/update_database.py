#!/usr/bin/env python

"""
This script adds entries to an ASE database.
It expects files in vasprun.xml format as input.
"""

import os
import argparse
from ase.db import connect
from ase.io import read
from supplemental_vasp_parser import read_additional_data_from_OUTCAR


def get_VBM_CBM(calc):
    VBM = -1.0e16
    CBM = 1.0e16
    for kpt in calc.kpts:
        for en, occ in zip(kpt.eps_n, kpt.f_n):
            if occ > 1.0 and en > VBM:
                VBM = en
            elif occ < 1.0 and en < CBM:
                CBM = en
    return VBM, CBM


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('database',
                        help='ASE database file')
    parser.add_argument('files',
                        nargs='+', action='append',
                        help='input files in vasprun.xml format')
    parser.add_argument('-f', '--firstframe',
                        action='store_true',
                        help='read and store first frame from input file'
                        ' (rather than the last, which is the default)')
    args = parser.parse_args()

    db = connect(args.database)
    for fname in args.files[0]:
        bname = os.path.basename(fname)
        print('fname: {}'.format(fname))
        try:
            entry = db.get(filename=bname)
            print('entry no. {} has the same filename'.format(entry.id))
        except KeyError:
            print('adding')
            if not args.firstframe:
                conf = read(fname, format='vasp-xml')
            else:
                conf = read(fname, 0, format='vasp-xml')

            # core levels
            outcar = fname.replace('run_', 'OUTCAR.').replace('.xml', '')
            properties = read_additional_data_from_OUTCAR(outcar)

            # KS eigen energies
            kpts = []
            for kpt in conf.calc.kpts:
                kpts.append({'coordinate': conf.calc.ibz_kpts[kpt.k-1],
                             'energies': kpt.eps_n, 'occupations': kpt.f_n,
                             'weight': kpt.weight, 'spin': kpt.s})
            properties['kpoints'] = kpts

            id = db.write(conf,
                          data=properties,
                          filename=bname)

            # valence and conduction band edges
            try:
                VBM, CBM = conf.calc.get_homo_lumo()
            except:
                try:
                    VBM, CBM = get_VBM_CBM(conf.calc)
                except:
                    VBM, CBM = -1, -1
            db.update(id, VBM=VBM, CBM=CBM)
