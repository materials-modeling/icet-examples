# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from collections import OrderedDict
from icet import ClusterExpansion
from numpy import array, count_nonzero
import mplpub
from matplotlib.ticker import FormatStrFormatter

# step 1: Collect ECIs in dictionary
ce = ClusterExpansion.read('ardr_models/Ag-Pd_ensemble_ardr-cve')
ecis = OrderedDict()
for order in range(len(ce.cluster_space.cutoffs) + 2):
    for orbit in ce.cluster_space.orbit_data:
        if orbit['order'] != order:
            continue
        if order not in ecis:
            ecis[order] = {'radius': [], 'parameters': []}
        ecis[order]['radius'].append(orbit['radius'])
        ecis[order]['parameters'].append(ce.parameters[orbit['index']])

orders = {2: '(a) Pairs', 3: '(b) Triplets', 4: '(c) Quadruplets'}

# step 2: Plot ECIs
mplpub.setup(height=2.3, width=3.5)
fig, axs = plt.subplots(1, 3, sharey=True)
plt.subplots_adjust(left=0.12, bottom=0.17, right=0.995,
                    top=0.99, wspace=0)
small_fontsize = 7

xpos = 0.08

colors = {2: 'green', 3: 'orange', 4: 'blue'}

for k, (order, data) in enumerate(ecis.items()):
    if k < 2 or k > 4:
        continue
    ax = axs[k - 2]
    ax.set_ylim((-5.5, 46))
    xmin = min(data['radius'])
    xmax = max(data['radius'])
    d = xmax - xmin
    xlims = (xmin - 0.16*d, xmax + 0.16*d)
    ax.set_xlim(xlims)
    if order == 2:
        ax.set_ylabel(r'Effective cluster interaction (meV)')
        # ax.text(0.05, 0.55, 'zerolet: {:.1f} meV'
        #        .format(1e3*ecis[0]['parameters'][0]),
        #        transform=ax.transAxes)
        # ax.text(0.05, 0.45, 'singlet: {:.1f} meV'
        #        .format(1e3*ecis[1]['parameters'][0]),
        #        transform=ax.transAxes)
    ax.tick_params('both', length=3, which='major')
    ax.plot([0, 10], [0, 0], color='black', linestyle='--', linewidth=0.5)
    ax.bar(data['radius'], 1e3 * array(data['parameters']), width=1.22*d/35,
            color=mplpub.tableau[colors[k]])
    #ax.scatter(data['radius'], len(data['radius']) * [-5],
    #           marker='o', s=2.0)
    ax.text(xpos, 0.90, orders[order],
            transform=ax.transAxes, fontsize=small_fontsize+1)
    ax.text(xpos, 0.82, r'{} zero ECIs'.format(len(data['parameters']) - count_nonzero(data['parameters'])),
            transform=ax.transAxes, fontsize=small_fontsize+1)
    ax.text(xpos, 0.74, r'{} non-zero ECIs'
            .format(count_nonzero(data['parameters'])),
            transform=ax.transAxes, fontsize=small_fontsize+1)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
axs[1].set_xlabel(r'Cluster radius (\AA)')
plt.savefig('figs/ecis.pdf')
plt.show()
