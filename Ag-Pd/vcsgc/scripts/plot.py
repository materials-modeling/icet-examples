import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.patches import Rectangle
import pandas as pd
import mplpub
from scipy.integrate import trapz
import numpy as np
import json
from icet.tools import ConvexHull
from scipy.optimize import curve_fit
from ase.units import kB
from phase_diagram import get_phase_diagram_data # bulk-alloys/experimental-phase-diagrams/scripts


def redlich_kister(x, *coeffs):
    y = x - x
    for p in range(len(coeffs)):
        y += coeffs[p] * (1 - 2 * x)**p
    y *= x * (1 - x)
    return y


def free_energy(c, T, polys):
    F = 0
    degree = len(polys) - 1
    coeffs = [poly(T) for poly in polys]
    F = redlich_kister(c, *coeffs)
    F -= 1000 * T * get_configurational_entropy(c)
    return F


def get_gap(T, concentrations, polys):
    hull = ConvexHull(concentrations, free_energy(concentrations, T, polys))
    c_previous = concentrations[0]
    for c in hull.concentrations:
        if c - c_previous > 1.01 * concentrations[1] - concentrations[0]:
            return c_previous, c
        c_previous = c
    return (-1, -1)


def get_configurational_entropy(concentrations):
    concentrations = np.array(concentrations)
    S = concentrations * np.log(concentrations)
    S = np.nan_to_num(S)
    S += (1 - concentrations) * np.log(1 - concentrations)
    S = np.nan_to_num(S)
    return -kB * S


df_vcsgc = pd.read_csv('collected-data/average.csv', delimiter='\t')
df_sgc = pd.read_csv('../AgPd-icet-ensemble-ardr-5-sgc/collected-data/average.csv', delimiter='\t')

mplpub.setup(height=6.7, width=3.5)
fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True)

temperatures_plot = [400, 800]

colors = {}
colors[temperatures_plot[0]] = mplpub.tableau['blue']
colors[temperatures_plot[1]] = mplpub.tableau['red']
colors_sgc = {}
colors_sgc[temperatures_plot[0]] = mplpub.tableau['lightblue']
colors_sgc[temperatures_plot[1]] = mplpub.tableau['lightred']
pd_color = '#107869'
pd_color_bg = '#afeee5'
ensemble_average_color = '#f38800'
ensemble_color = '#f3b100'

# this is an inset axes over the main axes
ax_inset = [None, None]
ax_inset[0] = inset_axes(ax[0], width='62%', height='64%',
                         bbox_to_anchor=(0.4, 0.07, 0.9, 0.8),
                         bbox_transform=ax[0].transAxes, loc=3)
ax_inset[1] = inset_axes(ax[1], width='57%', height='57%',
                         bbox_to_anchor=(0.14, 0.5, 0.9, 0.8),
                         bbox_transform=ax[1].transAxes, loc=3)

# Plot free energy derivatives
for T in temperatures_plot:
    # SGC
    df = df_sgc.loc[df_sgc.temperature == T].sort_values('Pd_concentration')
    zorder = 10 - T // 100
    kwargs = {'color': colors_sgc[T],
              'zorder': zorder, 'linewidth': 4.5}
    # If there is a gap, find it and plot separately
    if T == min(temperatures_plot):
        concentrations = np.array(df.Pd_concentration)
        dmu = -1e3*np.array(df.mu_Ag)
        c_bef = concentrations[0]
        for i, c_now in enumerate(concentrations):
            if c_now - c_bef > 0.1:
                i_break = i
                break
            c_bef = c_now
        ax[0].plot(concentrations[:i_break], dmu[:i_break], label='SGC, {} K'.format(T), **kwargs)
        ax[0].plot(concentrations[i_break:], dmu[i_break:], **kwargs)
        ax_inset[0].plot(concentrations[:i_break], dmu[:i_break], **kwargs)
        ax_inset[0].plot(concentrations[i_break:], dmu[i_break:], **kwargs)
    else:
        ax[0].plot(df.Pd_concentration, -1e3 * df.mu_Ag, label='SGC, {} K'.format(T), **kwargs)
        ax_inset[0].plot(df.Pd_concentration, -1e3 * df.mu_Ag, **kwargs)

    # VCSGC
    zorder = 11 - T // 100
    df = df_vcsgc.loc[df_vcsgc.temperature ==
                      T].sort_values('Pd_concentration')
    ax[0].plot(df.Pd_concentration, 1e3 * df.free_energy_derivative,
               label='VCSGC, {} K'.format(T), color=colors[T], zorder=zorder)
    ax_inset[0].plot(df.Pd_concentration, 1e3 * df.free_energy_derivative,
                     color=colors[T], zorder=zorder)

# Plot free energy of mixing
for T in temperatures_plot:
    df = df_vcsgc.loc[df_vcsgc.temperature ==
                      T].sort_values('Pd_concentration')
    dFdc = np.array(df.free_energy_derivative)
    c = np.array(df.Pd_concentration)
    F = []
    for j in range(len(c)):
        F.append(trapz(dFdc[:j + 1], x=c[:j + 1]))
    F_mix = np.array(F) - c * F[-1] - (1 - c) * F[0]
    ax[1].plot(c, 1e3 * F_mix, color=colors[T])

    # Plot inset figure
    c_ind = np.abs(c - 0.5).argmin()
    c_end = c[c_ind]
    dc = 1 - c_end
    F_mix_s = np.array(F) - (c - c_end) / dc * \
        F[-1] - (1 - c) / dc * F[c_ind]
    ax_inset[1].plot(c[c_ind:], 1e3 * F_mix_s[c_ind:], color=colors[T])

    # Plot convex hull line
    if T == min(temperatures_plot):
        hull = ConvexHull(c, F_mix_s)
        c_bef = hull.concentrations[0]
        for i, c_now in enumerate(hull.concentrations):
            if c_now - c_bef > 0.05:
                i_min = i - 1
                i_max = i
                break
            c_bef = c_now

        hull_concentrations = [hull.concentrations[
            i_min], hull.concentrations[i_max]]
        ax_inset[1].plot(hull_concentrations,
                         [1e3 * hull.energies[i_min], 1e3 * hull.energies[i_max]],
                         marker='o', color=colors[T], linestyle='--', zorder=20)

    # Plot dots and lines connecting the two plots
    ax[1].scatter([c[c_ind]], [1e3 * F_mix[c_ind]],
                  color=mplpub.tableau['grey'], zorder=10, marker='>')
    ax[1].scatter([1.0], [0.0], color=mplpub.tableau[
                  'grey'], zorder=10, marker='<')
    ax_inset[1].scatter([0.5], [0], color=mplpub.tableau[
                        'grey'], zorder=10, marker='>')
    ax_inset[1].scatter([1.0], [0], color=mplpub.tableau[
                        'grey'], zorder=10, marker='<')
    # ax[1].plot([c[c_ind], 0.19], [1e3 * F_mix[c_ind], 0.05],
    #           color=mplpub.tableau['grey'], zorder=10, alpha=0.5, linestyle=':')
    # ax[1].plot([1.0, 0.62], [0, 0.05], color=mplpub.tableau[
    #           'grey'], zorder=10, alpha=0.5, linestyle=':')

deltax = 0.01
ax[0].set_xlim([0 - deltax, 1 + deltax])
ax[0].set_ylim([1e3 * -0.65, 1e3 * 0.32])


ax[1].set_xlim([0 - deltax, 1 + deltax])
ax[1].set_ylim([-105, 15])

xmin = 0.48
xmax = 1.02
ymin = 1e3 * 0.06
ymax = 1e3 * 0.24
ax_inset[0].set_xlim([xmin, xmax])
ax_inset[0].set_ylim([ymin, ymax])
rect = Rectangle((xmin, ymin), width=xmax - xmin, height=ymax - ymin,
                 fill=False, color='black', linestyle='--', linewidth=0.5, zorder=10)
ax[0].add_patch(rect)

ax[0].plot([xmin, 0.42], [ymin, 1e3 * -0.06], linestyle='--',
           color='black', linewidth=0.5)
ax[0].plot([xmax, 0.985], [ymin, 1e3 * -0.06],
           linestyle='--', color='black', linewidth=0.5)

plt.subplots_adjust(left=0.16, bottom=0.06, right=0.97,
                    top=0.95, hspace=0.05)

ax[0].legend(bbox_to_anchor=(0.9, 1.2), bbox_transform=ax[0].transAxes, ncol=2)

ax[2].set_ylim([200, 900])

ax[0].set_ylabel('Free energy derivative (meV/atom)')
ax[1].set_ylabel('Free energy of mixing (meV/atom)')

ax_inset[1].set_yticks([])



# ==============================================
# Plot phase diagram
N_coeff = 3
N_RK = 4

ensembles = []
ensembles.append('cve')
ensembles.append('average')
ensembles += ['{:03d}'.format(i) for i in range(10)]


for ensemble in ensembles:
    print(ensemble)
    with open('free-energies/{}.json'.format(ensemble), 'r') as f:
        data = json.load(f)

    Ls = [[] for i in range(N_RK)]
    temperatures = []

    # Fit free energy curves to Redlich-Kister polynomials
    for T in range(100, 900, 25):
        temp = str(T)
        conc = data[temp][0]
        G_exc = data[temp][1] + T * 1000 * get_configurational_entropy(conc)
        L, _ = curve_fit(redlich_kister, conc, G_exc, p0=[1] * N_RK)
        temperatures.append(T)

        for i in range(N_RK):
            Ls[i].append(L[i])

    # Fit Redlich-Kister expansion coefficients to polynomial
    polys = []
    plot_temps = np.linspace(0, 1000, 1001)
    for i in range(N_RK):
        coeff = np.polyfit(temperatures, Ls[i], N_coeff)
        polys.append(np.poly1d(coeff))

    # Identify miscibility gap
    concentrations = np.linspace(0.4, 1, 701)
    gap_left = []
    gap_right = []
    temperatures = np.linspace(0, 1000, 2001)
    for T in temperatures:
        left, right = get_gap(T, concentrations, polys)
        if left < -1e-3:
            break
        else:
            gap_left.append(left)
            gap_right.append(right)

    # Identify points to be plotted
    if ensemble == 'cve':
        i_left = np.abs(temperatures-min(temperatures_plot)).argmin()
        i_right = np.abs(temperatures-min(temperatures_plot)).argmin()
        gaps_plot = [gap_left[i_left], gap_right[i_right]]

    new_temperatures = list(temperatures[:len(gap_left)])
    for c, T in zip(reversed(gap_right), reversed(temperatures[:len(gap_right)])):
        gap_left.append(c)
        new_temperatures.append(T)

    if ensemble == 'cve':
        ax[2].plot(gap_left, new_temperatures, color=pd_color,
                   label='Cross-validation estimator, final CE', linewidth=1.9, zorder=20)
        ax[2].fill_between(gap_left, 0, new_temperatures,
                           facecolor=pd_color_bg, alpha=0.25)

    elif ensemble == 'average':
        ax[2].plot([-1, -1], [-1, -1], color=ensemble_color,
           linewidth=0.5, alpha=1.0, label='Ensemble optimizer, individual CEs')

        ax[2].plot(gap_left, new_temperatures, color=ensemble_average_color,
                   label='Ensemble optimizer, average CE', linewidth=1.7)
    else:
        ax[2].plot(gap_left, new_temperatures,
                   color=ensemble_color, linewidth=0.5)


'''
# Plot inset figure
c = np.arange(0, 1.01, 0.01)
F = free_energy(c, max(temperatures_plot), polys)
c_ind = np.abs(c - 0.5).argmin()
c_end = c[c_ind]
dc = 1 - c_end
F_mix_s = np.array(F) - (c - c_end) / dc * \
    F[-1] - (1 - c) / dc * F[c_ind]
ax_inset[1].plot(c[c_ind:], F_mix_s[c_ind:], color=colors[max(temperatures_plot)])
'''


exp_data, _, _, _ = get_phase_diagram_data(
    '../../../experimental-phase-diagrams/data/AgPd-DinWatKro08/')
ax[2].plot(exp_data['solvus'][0], exp_data['solvus'][1], ':',
           color='black', linewidth=1.5, label='Experiment')

T = max(temperatures_plot)
ax[2].plot([-0.1, 1.1], [T, T], color=colors[T])

T = min(temperatures_plot)
ax[2].plot([-0.1, gaps_plot[0]], [T, T], color=colors[T], zorder=25)
ax[2].plot([gaps_plot[0], gaps_plot[1]], [T, T], color=colors[T], linestyle='--', zorder=25)
ax[2].plot([gaps_plot[1], 1.1], [T, T], color=colors[T], zorder=25)
ax[2].scatter(gaps_plot, [T, T], color=colors[T], marker='o', zorder=25)

ax[2].set_ylabel('Temperature (K)')
ax[2].set_xlabel('Pd concentration')

ax[2].set_xlim([0 - deltax, 1 + deltax])

ax[2].legend(handlelength=1.8, bbox_to_anchor=(
    -0.01, 0.86), loc="upper left", bbox_transform=ax[2].transAxes)

xpos = 0.016
ypos = 0.92
ax[0].text(xpos, ypos, '(a)', transform=ax[0].transAxes)
ax[1].text(xpos, ypos, '(b)', transform=ax[1].transAxes)
ax[2].text(xpos, ypos, '(c)', transform=ax[2].transAxes)


plt.savefig('pdf/free-energy-showcase.pdf')
plt.show()
