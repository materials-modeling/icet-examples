import pandas as pd
from scipy.integrate import trapz
import numpy as np
import json

ces = ['{:03d}'.format(i) for i in range(10)]
ces += ['average', 'cve']

for ce in ces:
    df = pd.read_csv('collected-data/{}.csv'.format(ce), delimiter='\t')

    data = {}

    for T in sorted(df.temperature.unique()):
        df_T = df.loc[df.temperature == T].sort_values('Pd_concentration')

        F = []
        for j in range(len(df_T.Pd_concentration)):
            F.append(trapz(df_T.free_energy_derivative[:j + 1],
                           x=df_T.Pd_concentration[:j + 1]))

        F_mix = np.array(F) - \
                    np.array(df_T.Pd_concentration) * F[-1] - \
                    (1 - np.array(df_T.Pd_concentration)) * F[0]
        data[str(T)] = (list(df_T.Pd_concentration), list(1e3 * F_mix))

    with open('free-energies/{}.json'.format(ce), 'w') as f:
        json.dump(data, f)