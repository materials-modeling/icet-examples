from ase.build import make_supercell
from numpy import arange, array
from icet import ClusterExpansion
from mchammer.calculators import ClusterExpansionCalculator
from mchammer.ensembles import VCSGCEnsemble
from multiprocessing import Pool
import argparse
import time

parser = argparse.ArgumentParser(
    description='Run Monte Carlo simulation in parallel.'
)
parser.add_argument('CE', type=str,
                    help='Cluster expansion model')
parser.add_argument('T_L', type=int,
                    help='Lower temperature bound (inclusive)')
parser.add_argument('dT', type=int,
                    help='Length of temperature interval (T_L + dT is non-inclusive)')
in_args = parser.parse_args()

ce_tag = in_args.CE.replace('ardr_models/', '').replace('Ag-Pd_ensemble_ardr-', '')
ce = ClusterExpansion.read('ardr_models/Ag-Pd_ensemble_ardr-{}'.format(ce_tag))
chemical_symbols = ce.cluster_space.chemical_symbols
prim_structure = ce.cluster_space.primitive_structure
prim_structure.set_chemical_symbols(['Ag'])
atoms = make_supercell(prim_structure,
                       5 * array([[-1, 1, 1],
                                  [1, -1, 1],
                                  [1, 1, -1]]))
calculator = ClusterExpansionCalculator(atoms, ce)

print('Running {} between {} and {}'.format(in_args.CE, in_args.T_L, in_args.T_L + in_args.dT))

def run_simulation(args):
    start = time.time()
    temperature = args[0]
    phi = args[1]
    print("Running {} and {:.3f}".format(temperature, phi))
    mc = VCSGCEnsemble(
        calculator=calculator,
        atoms=atoms,
        ensemble_data_write_interval=200,
        trajectory_write_interval=200,
        temperature=temperature,
        phis={'Ag': phi,
              'Pd': -2 - phi},
        kappa=200.0,
        data_container='mc-data/vcsgc-{}-T{}-phi{:.3f}.dc'.format(ce_tag, temperature, phi)
    )
    mc.run(number_of_trial_steps=100000)
    print('Finished {} and {:.3f} in {:.2f} s'.format(temperature, phi, time.time() - start))

args = []
for temperature in range(in_args.T_L, in_args.T_L + in_args.dT, 25):
    for phi in arange(-2.3, 0.325, 0.025):
        args.append([int(temperature), float(phi)])

pool = Pool(processes=20)
pool.map(run_simulation, args)
