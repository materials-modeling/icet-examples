from glob import glob
from mchammer import DataContainer
import pandas as pd

equilibration = 10000
ces = ['{:03d}'.format(i) for i in range(5, 10)]
ces.append('average')
ces.append('cve')
for ce in ces:
    data = []
    for fname in glob('mc-data/vcsgc-{}-*.dc'.format(ce)):
        print(fname)
        dc = DataContainer.read(fname)
        data_row = dc.ensemble_parameters
        data_row['fname'] = fname
        n_atoms = data_row['n_atoms']
        
        data_row['Pd_concentration'] = \
                                       dc.get_average('Pd_count', start=equilibration) / n_atoms
        data_row['mixing_energy'] = \
                                    dc.get_average('potential', start=equilibration) / n_atoms
        data_row['acceptance_ratio'] = \
                                       dc.get_average('acceptance_ratio', start=equilibration)
        data_row['free_energy_derivative'] = \
                                             dc.get_average('free_energy_derivative', start=equilibration)
        data.append(data_row)

    df = pd.DataFrame(data)
    df.to_csv('collected-data/{}.csv'.format(ce), sep='\t')
