for i in average cve;
do
    echo $i
    sbatch -J sgc-$i submit.job ardr_models/Ag-Pd_ensemble_ardr-$i 700 225 # if they dont start at the same time we'd better start with this
    for T in 100 300 500;
    do
        sbatch -J sgc-$i submit.job ardr_models/Ag-Pd_ensemble_ardr-$i $T 200
    done
done
