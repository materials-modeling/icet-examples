from glob import glob
from mchammer import DataContainer
import matplotlib.pyplot as plt
import pandas as pd
import scipy as sp
import numpy as np

df = pd.read_csv('collected_data.csv', sep='\t')

# Calculate and plot free energy of mixing
fig, ax = plt.subplots()

for T in sorted(df.temperature.unique()):
    df_T = df.loc[df['temperature'] == T].sort_values('Pd_concentration')

    dFdc = np.array(df_T.free_energy_derivative)
    c = np.array(df_T.Pd_concentration)
    F = []
    for j in range(len(c)):
        F.append(sp.integrate.trapz(dFdc[:j + 1], x=c[:j + 1]))
    F_mix = np.array(F) - c * F[-1] - (1 - c) * F[0]

    ax.plot(c, F_mix, label='{} K'.format(T))

ax.set_xlabel('H concentration')
ax.set_ylabel('Free energy of mixing (meV/atom)')
ax.set_xlim([-0.02, 1.02])
ax.legend()
plt.tight_layout()
plt.show()
plt.savefig('free-energy-{}.pdf')
