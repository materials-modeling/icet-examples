The cluster expansions in `sgc/ardr_models` and `vcsgc/ardr_models`
are exactly the same. The analaysis was primarily carried out in the
`vcsgc` folder. Raw Monte Carlo data can be found on SweStore under
`cms/users/rmagnus/icet-paper-AgPd-MC/`.