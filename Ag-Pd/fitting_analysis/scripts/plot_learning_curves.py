import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import mplpub

# parameters
cutoffs = [13.0, 6.5, 6.0]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')

# read data
df_l2 = pd.read_pickle(f'data/df_learning_curve_least-squares_{cutoffs_str}.pickle')  # noqa
df_rfe = pd.read_pickle(f'data/df_learning_curve_rfe-l2_{cutoffs_str}.pickle')
df_ardr = pd.read_pickle(f'data/df_learning_curve_ardr_{cutoffs_str}.pickle')
df_lasso = pd.read_pickle(f'data/df_learning_curve_lasso_{cutoffs_str}.pickle')


# skip last points as these are very close to 100% training structures
df_l2 = df_l2[:-2]
df_rfe = df_rfe[:-2]
df_lasso = df_lasso[:-2]
df_ardr = df_ardr[:-2]


# plot
mplpub.setup(template='natcom', height=2.9, width=3.2)
fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

gs = gridspec.GridSpec(2, 1)
gs.update(wspace=0.0, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])


# least squares
color = 'k'
ax1.plot(df_l2.train_size, 1000 * df_l2.rmse_validation,
         '-v', color=color, label='SVD')
ax2.plot(df_l2.train_size, df_l2.nzp_ave,
         '-v', color=color, label='SVD')

# RFE
color = 'goldenrod'
ax1.plot(df_rfe.train_size, 1000 * df_rfe.rmse_validation,
         '-^', color=color, label='RFE with OLS')
ax2.plot(df_rfe.train_size, df_rfe.nzp_ave,
         '-^', color=color, label='RFE with OLS')

# ARDR
color = 'forestgreen'
ax1.plot(df_ardr.train_size, 10 * df_ardr.rmse_validation,
         '-s', color=color, label='ARDR')
ax2.plot(df_ardr.train_size, df_ardr.nzp_ave,
         '-s', color=color, label='ARDR')

# LASSO
color = 'cornflowerblue'
ax1.plot(df_lasso.train_size, 1000 * df_lasso.rmse_validation,
         '-o', color=color, label='LASSO')
ax2.plot(df_lasso.train_size, df_lasso.nzp_ave,
         '-o', color=color, label='LASSO')

ax1.set_ylabel('RMSE (meV/atom)')
ax2.set_xlabel('Number of training structures')
ax2.set_ylabel('Number of features')


for ax in [ax1, ax2]:
    ax.tick_params()
    ax.set_xlim([45.0, 560.0])

ax1.set_xticklabels([])
ax1.legend(loc=1)

ax1.set_ylim([1.75, 4.0])
ax2.set_ylim([0.0, 65.0])
ax1.text(x=0.04, y=0.88, s='a)', transform=ax1.transAxes)
ax2.text(x=0.04, y=0.88, s='b)', transform=ax2.transAxes)

fig.tight_layout()
fig.savefig('pdf/fitting_learning_curves.pdf')
