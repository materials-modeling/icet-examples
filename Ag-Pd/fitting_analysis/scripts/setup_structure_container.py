import os
from ase.db import connect
from icet import ClusterSpace, StructureContainer

# parameters
cutoffs = [13.0, 6.5, 6.0]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')

sc_fname = 'structure_containers/sc_{}.sc'.format(cutoffs_str)

if os.path.isfile(sc_fname):
    print('already exists, skipping', sc_fname)

# step 1: Basic setup
db = connect('structures/reference_data.db')
prim = db.get(id=1).toatoms()  # primitive structure

# step 2: Set up the basic structure and a cluster space
cs = ClusterSpace(atoms=prim, cutoffs=cutoffs, chemical_symbols=['Ag', 'Pd'])
print(cs)

# step 3: Parse the input structures and set up a structure container
sc = StructureContainer(cluster_space=cs)
for row in db.select('natoms<=8'):
    sc.add_structure(atoms=row.toatoms(), user_tag=row.tag,
                     properties={'mixing_energy': row.mixing_energy})
print(sc)

# write
sc.write(sc_fname)
