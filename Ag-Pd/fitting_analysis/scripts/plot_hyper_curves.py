import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mplpub

# parameters
cutoffs = [13.0, 6.5, 6.0]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')


# read data
df_lasso = pd.read_pickle('data/df_hyper_lasso_{}.pickle'.format(cutoffs_str))
df_ardr = pd.read_pickle('data/df_hyper_ardr_{}.pickle'.format(cutoffs_str))
df_rfe = pd.read_pickle('data/df_hyper_rfe_{}.pickle'.format(cutoffs_str))

# make rfe less dense
df_rfe = df_rfe[::3]


# plotting
mplpub.setup(template='natcom', height=2.2, width=3.2)

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

# RFE
color = 'goldenrod'
ax1.plot(df_rfe.nzp_ave, 1000*df_rfe.rmse_validation,
         '-^', color=color, label='RFE with OLS')

# ARDR
color = 'forestgreen'
ax1.plot(df_ardr.nzp_ave, 10*df_ardr.rmse_validation,
         '-s', color=color, label='ARDR')

# LASSO
color = 'cornflowerblue'
ax1.plot(df_lasso.nzp_ave, 1000*df_lasso.rmse_validation,
         '-o', color=color, label='LASSO')


ax1.set_xlabel('Number of features')
ax1.set_ylabel('RMSE (meV/atom)')
ax1.legend(loc=1)

ax1.set_yticks(np.arange(1.0, 10.0, 0.5))
ax1.set_xlim([0.0, 85.0])
ax1.set_ylim([1.75, 4.2])
ax1.text(x=0.03, y=0.92, s='c)', transform=ax1.transAxes)

fig.tight_layout()
fig.savefig('pdf/fitting_hyper_curves.pdf')
