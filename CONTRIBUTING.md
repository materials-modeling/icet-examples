Contribution guidelines
=======================

General guidelines
------------------

* **Read the contribution guidelines**.<br>
  You have done well so far but keep on reading all the way to the **bottom of
  this page**.

* **Remember and apply the contribution guidelines!**<br>
  [(random user guideline quizzing can occur at any
  time)](http://www.ohsinc.com/services/employee-drug-testing/)

* Use [expressive function and variable names](https://xkcd.com/910/), e.g.,
  * Good: `get_number_of_structures`
  * Avoid: `get_nbr_struct` or any variation thereof
  * Good: `get_number_of_allowed_elements`
  * Avoid: `get_Mi` or any variation thereof
  * Avoid anything remotely resembling `int mytmp = 13`

* Document and comment, document and comment, document and comment, document
  and comment, document and comment ... (it cannot be said too often).<br>
  Language specific guide lines are given below.

* Always imagine someone else has to be able to read your code. Hence do your
  best at writing [*clean and structured* code](https://www.xkcd.com/1513/).
  * Avoid commenting out blocks of code for "later use/reference" in commits.


### Python

Code should be [pep8](https://www.python.org/dev/peps/pep-0008/) compliant and
pass [pyflakes](https://pypi.python.org/pypi/pyflakes). (Eventually,
pep8/pyflakes will be added to the CI, at which point code *must* be
compliant.)

Any functions/functionality *must* be properly documented. This includes
[docstrings](https://en.wikipedia.org/wiki/Docstring) for functions, classes,
and modules that clearly describe the task performed, the interface (where
necessary), the output, and if possible an example. atomicrex uses [NumPy Style
Python Docstrings](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html).

When in doubt ask the main developers. Also [the coding conventions from
ASE](https://wiki.fysik.dtu.dk/ase/development/python_codingstandard.html)
provide useful guidelines.

Good job, you are still reading! [Will you make it to the
end?](https://xkcd.com/169/)


### Please use spaces

While you are entitled to [your own
opinion](http://lea.verou.me/2012/01/why-tabs-are-clearly-superior/) this
project uses spaces instead of tabs. Even if you are geeky enough to care and
like [Silicon Valley](https://www.youtube.com/watch?v=SsoOG6ZeyUI) you should
know that [developers who use spaces make more
money](https://stackoverflow.blog/2017/06/15/developers-use-spaces-make-money-use-tabs/).
Also the use of spaces is strongly recommended by our beloved
[pep8](https://www.python.org/dev/peps/pep-0008/) standard.



Project management guidelines
-----------------------------

To organize the tasks in this project we use the functionalities provided by
the issue tracker of GitLab. Please adhere to the workflow described below.

### What is a task?

A task should be a small self-contained unit that can be typically completed
within a few days.


### How to set up a task

If you have identified a need
* create an issue in GitLab,
* assign labels ("Analysis", "Calculation", "Development" etc), and
* invite other team members to comment on this issue.
* do *not* assign the issue to a member yet (including yourself)
* do *not* move the issue onto the board (that is leave it in the backlog)


### Initial review

Once input has been collected a decision should be made by the project manager
in communication with other team members on how to proceed with the issue. The
description of the issue will be updated, if possible including a "DEMO"
section that describes which conditions need to be fulfilled in order to pass
the final review.

Then the issue will be
* placed on the board (usually the `To Do` column),
* possibly assigned a priority, and
* assigned to one or more team members for handling.


### How to work on a task

To pick up an assigned task (issue) it has to be moved from the "To Do"
column of the board to the "Doing" column (this will switch the labels
automatically). Alternatively one can set the labels by hand, which will
update the positioning on the board.

Note that the order of the issues in the columns of the board should reflect
the order in which they need to be addressed. Thus pick from the top.

When working on an issue remember

 **Commit frequently. Push often**

 **Commit frequently. push often**

 **Commit frequently. push often**


### How to finalize (close) a task

Once a task is considered complete by the person(s) working on it, it should
undergo a final review. To this end, the task (issue) has to be
* moved from the "Doing" column to the "Review" column of the board and
* assigned to another team member for review.

The reviewer should consider the description of the issue, ideally resorting to
the "DEMO" section (see above) to decide whether the initial goal has been
reached (or at least appropriately addressed). In this process, the original
assignee(s) and the reviewer are encouraged to engage in a discussion if
necessary to resolve potential conflicts, misunderstandings, or omissions.

Once the reviewer is satisfied she/he can close the issue.


Commit messages
---------------

When writing commit messages, generating issues, or submitting merge requests,
[write meaningful and concise commit messages](https://xkcd.com/1296/). Also
please use the following prefixes to identify the category of your
commit/issue/merge request.

* BLD: change related to building
* BUG: bug fix
* DATA: general data
* DOC: documentation
* ENH: enhancement
* MAINT: maintenance commit (refactoring, typos, etc.)
* STY: style fix (whitespace, PEP8)
* TST: addition or modification of tests
* REL: related to releases

Less common:
* API: an (incompatible) API change
* DEP: deprecate something, or remove a deprecated object
* DEV: development tool or utility
* FIG: images and figures
* REV: revert an earlier commit

The first line should not exceed 78 characters. If you require more space,
insert an empty line after the "title" and add a longer message below. In this
message, you should again limit yourself to 78 characters *per* line.

Hint: If you are using emacs you can use ``Meta``+``q``
[shortcut](https://shortcutworld.com/en/Emacs/23.2.1/linux/all) to "reflow the
text". In sublime you can achieve a similar effect by using ``Alt``+``q`` (on
Linux)/ ``Alt``+``Cmd``+``q`` (on MacOSX).
