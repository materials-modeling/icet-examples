import matplotlib.pyplot as plt
import numpy as np
from os.path import basename

try:
    import mplpub
    mplpub.setup(template='natcom', height=2.2, width=3.2)  # noqa
except ModuleNotFoundError:
    pass


# n_params, lmbda, rmse_validation, rms_val_std, rmse_train,
# n_zero_params, n_zero_params_std, cutoffs [5.4, 5.4]
lasso_data = np.loadtxt('data/lasso_alpha_scan.data')

# n_params, lmbda, rmse_validation, rmse_train,
# n_zero_params, n_zero_params_std, cutoffs [5.8, 5.8, 5.8]
ardr_data = np.loadtxt('data/ardr_lambda_scan2.data')

# n_params, n_features, rmse_validation, rms_val_std,
# rmse_train, n_zero_params, n_zero_params_std, cutoffs
rfe_data = np.loadtxt('data/rfe_scan.data')

fig, ax = plt.subplots()

ms = 3.5
ax.plot(ardr_data[:, 5], ardr_data[:, 2], '-s',
        markersize=ms, color='forestgreen', label='ARDR')
ax.plot(lasso_data[:, 5], 10*lasso_data[:, 2], '-o',
        markersize=ms, color='cornflowerblue', label='LASSO')
ax.plot(rfe_data[:, 5], 10*rfe_data[:, 2], '-^',
        markersize=ms, color='goldenrod', label=r'RFE with OLS')
ax.text(x=0.04, y=0.9, s='c)', transform=ax.transAxes)

ax.set_xlabel('Number of features')
ax.set_ylabel('RMSE (meV/site)')

ax.legend(loc='center')
ax.set_ylim(1.8, 4.2)
ax.set_xlim(0, 42)

plt.tight_layout()

outfile = basename(__file__)
outfile = outfile.replace('.py', '.pdf').replace('plot_', '')
plt.savefig('pdf/{}'.format(outfile), bbox_inches='tight')
