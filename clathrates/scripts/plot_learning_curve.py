import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
from os.path import basename

try:
    import mplpub
    mplpub.setup(template='natcom', height=2.9, width=3.2)  # noqa
except ModuleNotFoundError:
    pass


# n_params, alpha, train_size,rmse_validation, rmse_train,
# n_zero_params, n_zero_params_std, cutoffs: [5.8, 5.8, 5.8]
lasso_data = np.loadtxt('data/lasso_learning_curve.py')

# n_params, train_size, lmbda, rmse_validation, rmse_train,
# n_zero_params, n_zero_params_std, cutoffs [5.8, 5.8, 5.8]
ardr_data = np.loadtxt('data/ardr_learning_curve5.data')

# n_params, train_size, rmse_validation, rms_val_std,
# rmse_train, n_zero_params, n_zero_params_std, cutoffs [5.4, 5.4]
rfe_data = np.loadtxt('data/rfe_learning_curve.data')

gs = gridspec.GridSpec(2, 1)
gs.update(wspace=0.0, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])

# ax.plot(sb_data[:,2], 10*sb_data[:,3], '-o', label='Split-Bregman')
ms = 3.5
color = 'forestgreen'
ax1.plot(ardr_data[:, 1], ardr_data[:, 2], '-s',
         markersize=ms, color=color, label='ARDR')
ax2.plot(ardr_data[:, 1], ardr_data[:, 5], '-s',
         markersize=ms, color=color, label='ARDR')

color = 'cornflowerblue'
ax1.plot(lasso_data[:, 1], 10*lasso_data[:, 2], '-o',
         markersize=ms, color=color, label='LASSO')
ax2.plot(lasso_data[:, 1], lasso_data[:, 5], '-o',
         markersize=ms, color=color, label='LASSO')

color = 'goldenrod'
ax1.plot(rfe_data[:, 1], 10*rfe_data[:, 2], '-^',
         markersize=ms, color=color, label=r'RFE with OLS')
ax2.plot(rfe_data[:, 1], rfe_data[:, 5], '-^', markersize=ms,
         color=color, label=r'RFE with OLS')

ax1.set_ylabel('RMSE (meV/site)')
ax2.set_xlabel('Number of training structures')
ax2.set_ylabel('Number of features')

for ax in [ax1, ax2]:
    ax.set_xlim(0, 220)

ax1.legend()
ax1.set_xticklabels([])

ax1.set_ylim(1.8, 5.2)
ax2.set_ylim(0, 44)
ax1.text(x=0.04, y=0.86, s='a)', transform=ax1.transAxes)
ax2.text(x=0.04, y=0.86, s='b)', transform=ax2.transAxes)

plt.tight_layout()

outfile = basename(__file__)
outfile = outfile.replace('.py', '.pdf').replace('plot_', '')
plt.savefig('figs/{}'.format(outfile), bbox_inches='tight')
