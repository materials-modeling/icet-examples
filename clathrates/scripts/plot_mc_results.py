import pickle
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
from os.path import basename

try:
    import mplpub
    mplpub.setup(template='natcom', height=2.2, width=3.2)  # noqa
except ModuleNotFoundError:
    pass


with open('mc_data_size2.pickle', 'rb') as handle:
    mc_data = pickle.load(handle)

fig, ax = plt.subplots()

# Looping over CEs
for k, (ce_id, temp_dict) in enumerate(mc_data.items()):

    # Collect data
    energies = []
    sof_6c = []
    sof_16i = []
    sof_24k = []
    temps = []
    for T in sorted(temp_dict.keys()):
        energies.append(temp_dict[T]['energy']*1000/(1*54))
        sof_6c.append(temp_dict[T]['6c_sof'])
        sof_16i.append(temp_dict[T]['16i_sof'])
        sof_24k.append(temp_dict[T]['24k_sof'])
        temps.append(T)
    energies = np.array(energies)

    if sof_6c[0] < 99:
        print("skipping")
        continue
    # Plot data
    colors = ['#de2d26', '#3182bd', '#feb24c']
    if ce_id == 'mean':
        alpha = 1.0
        lw = 2
        ax.plot(temps, sof_6c, c=colors[0], alpha=alpha, lw=lw, label='6c')
        ax.plot(temps, sof_16i, c=colors[1], alpha=alpha, lw=lw, label='16i')
        ax.plot(temps, sof_24k, c=colors[2], alpha=alpha, lw=lw, label='24k')
    else:
        alpha = 0.1
        lw = 0.5
        ax.plot(temps, sof_6c, c=colors[0], alpha=alpha, lw=lw)
        ax.plot(temps, sof_16i, c=colors[1], alpha=alpha, lw=lw)
        ax.plot(temps, sof_24k, c=colors[2], alpha=alpha, lw=lw)

# 6c
# 11.5341550288  4.79456553247
# 24k
# 11.5371450581  1.79855620898
# 16i
# 11.5429106922  5.02139082025
colors = ['#de2d26', '#3182bd', '#feb24c']

ms = 9
marker = '*'
temperature = 850
ax.plot([], [],
        marker, c='#666666', ms=ms, alpha=0.8, label='Experiment')
ax.plot(temperature, 4.79456553247*100/6,
        marker, c=colors[0], ms=ms, alpha=0.8)
ax.plot(temperature, 5.02139082025*100/16,
        marker, c=colors[1], ms=ms, alpha=0.8)
ax.plot(temperature, 1.79855620898*100/24,
        marker, c=colors[2], ms=ms, alpha=0.8)

ax.legend(handlelength=2.0,
          loc='upper left', bbox_to_anchor=(0.0, 0.85))

ax.set_xlabel('Temperature (K)')
ax.set_ylabel('Al occupation (\%)')
ax.set_xlim(0, 1150)
# ax.set_ylim(-5, 118)
# ax.text(x=0.04, y=0.92, s='d)', transform=ax.transAxes)

outfile = basename(__file__)
outfile = outfile.replace('.py', '.pdf').replace('plot_', '')
plt.savefig('pdf/{}'.format(outfile), bbox_inches='tight')
