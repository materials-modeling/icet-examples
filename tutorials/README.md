# icet tutorials
You can launch an interactive version of this repository on
[Binder](https://mybinder.org/v2/gl/materials-modeling%2Ficet-examples/master)

## Prerequisites
A computer, preferably with *icet* and jupyter notebook already installed.
If not done so already, the latter can be installed as via
```
python3 -m pip install --user icet jupyter
```

## Learning objectives
In this tutorial using *icet*, a Python package that allows the construction and sampling of alloy cluster expansions (CEs), you will learn
* What CEs are and what they can be used for
* How to generate training sets for CE construction
* How to apply machine learning algorithms for training CEs
* How to sample a CE in various thermodynamic ensembles using Monte Carlo (MC) simulations
* How to efficiently analyze data from MC simulations

## Overview
This tutorial intends to give you an introduction to icet and cluster expansion construction.
The tutorial consists of 4 parts; each part consists of a jupyter notebook along with necessary files.
Each part can be run separately.
The parts consist of

* stage 1: Structure generation, enumeration and how-to map relaxed configurations to the ideal lattice.
* stage 2: Cluster expansion construction for the AgPd alloy using a provided ASE database calculated with DFT; The optimization algorithms we will investigate is ARDR, ordinary least-squares, LASSO and RFE
* stage 3: Sampling with a cluster expansion in the canonical and SGC ensemble for an Ising model
* stage 4: How to analyze Monte Carlo sampling using prepared densely sampled data generated using the Ising model.
