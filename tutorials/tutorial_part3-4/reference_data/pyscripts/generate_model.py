from ase.build import bulk
from icet import ClusterSpace, ClusterExpansion

# parameters
a0 = 3.0
cutoffs = [a0 + 0.1]
chemical_symbols = ['Si', 'Ge']
ising_ecis = [0, 0, -0.1]

# build model
prim = bulk('Si', 'sc', a=a0)
cs = ClusterSpace(prim, cutoffs, chemical_symbols)
ce = ClusterExpansion(cs, ising_ecis)
ce.write('cluster_expansions/Si_Ge_ising_model.ce')
print(ce)
