import glob
import pandas as pd
import numpy as np
from ase.units import kB
from mchammer import DataContainer
import matplotlib.pyplot as plt

import seaborn as sns
sns.set_context('talk')


# parameters
size = 12
eq_steps = 5000


# collect data
fnames = glob.glob('sgc_runs/dc_size{}_*.dc'.format(size))

records = []
for fname in fnames:

    # ensemble parameters
    dc = DataContainer.read(fname)
    n_atoms = dc.ensemble_parameters['n_atoms']
    mu_Si = dc.ensemble_parameters['mu_Si']
    T = dc.ensemble_parameters['temperature']

    # thermodynamic data
    energies = dc.get('potential', start=eq_steps)
    conc = dc.get('Si_count', start=eq_steps) / n_atoms
    acceptance_ratio = dc.get('acceptance_ratio', start=eq_steps)

    heat_capacity = np.var(energies) / (kB * T**2) / n_atoms
    energies /= n_atoms

    row = dict(temperature=T, mu_Si=mu_Si, c_Si=conc.mean(), c_Si_std=conc.std(),
               energy=energies.mean(), energy_std=energies.std(),
               heat_capacity=heat_capacity, acceptance_ratio=acceptance_ratio.mean())
    records.append(row)

df_all = pd.DataFrame(records)


# plotting
fig = plt.figure(figsize=(8, 5))
ax1 = fig.add_subplot(111)

for T in df_all.temperature.unique():
    df = df_all[df_all.temperature == T]
    df = df.sort_values(by='mu_Si')
    label = '{}K'.format(T)
    ax1.plot(df.c_Si, df.mu_Si, '-o', label=label)

ax1.set_xlabel('Concentration')
ax1.set_ylabel(r'$\mu_{Si}$')
ax1.set_xlim(df_all.c_Si.min(), df_all.c_Si.max())
ax1.legend(fontsize=12)

fig.tight_layout()
fig.savefig('pdf/sgc_mc.pdf')
plt.show()
