# icet examples

This repo contains examples and tutorials for the usage of icet.
In the examples Ag-Pd and clathrate directories the data and scripts used in the icet paper is provided.
In the `tutorials` directory a few jupyter notebook tutorials are provided.

## References

Ångqvist *et al.*,
*ICET – A Python Library for Constructing and Sampling Alloy Cluster Expansions*,
[Advanced Theory and Simulations, **2**, 1900015 (2019)](https://doi.org/10.1002/adts.201900015)

